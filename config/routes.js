const express = require("express");
const controllers = require("../app/controllers/api/v1");
const swaggerUI = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

const apiRouter = express.Router();

//Swagger UI Express
apiRouter.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));
// akun
apiRouter.post("/user/login", controllers.usersController.login);
apiRouter.post("/user/register", controllers.usersController.create);

// user
apiRouter.get(
  "/user",
  controllers.usersController.authorize,
  controllers.usersController.list
);
apiRouter.put(
  "/user/:id",
  controllers.usersController.authorize,
  controllers.usersController.update
);
apiRouter.put(
  "/user/approveadmin/:id",
  controllers.usersController.authorize,
  controllers.usersController.approveAdminRole
);
apiRouter.get(
  "/user/:id",
  controllers.usersController.authorize,
  controllers.usersController.show
);
apiRouter.get(
  "/user/profile/:id",
  controllers.usersController.authorize,
  controllers.usersController.showCurrent
);
apiRouter.delete(
  "/user/:id",
  controllers.usersController.authorize,
  controllers.usersController.destroy
);

// cars
apiRouter.get(
  "/cars",
  controllers.usersController.authorize,
  controllers.carsController.list
);

apiRouter.post(
  "/cars",
  controllers.usersController.authorize,
  controllers.carsController.create
);

apiRouter.put(
  "/cars/:id",
  controllers.usersController.authorize,
  controllers.carsController.update
);

apiRouter.get(
  "/cars/:id",
  controllers.usersController.authorize,
  controllers.carsController.show
);

apiRouter.delete(
  "/cars/:id",
  controllers.usersController.authorize,
  controllers.carsController.destroy
);

apiRouter.get(
  "/carshistory",
  controllers.usersController.authorize,
  controllers.carsController.historyCars
);

apiRouter.get("/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

// apiRouter.use(api.api.onLost);
// apiRouter.use(api.api.onError);

module.exports = apiRouter;
