"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  cars.init(
    {
      name: DataTypes.STRING,
      type: DataTypes.STRING,
      price: DataTypes.STRING,
      img: DataTypes.STRING,
      plate: DataTypes.STRING,
      manufacture: DataTypes.STRING,
      description: DataTypes.TEXT,
      transmission: DataTypes.STRING,
      year: DataTypes.STRING,
      options: DataTypes.STRING,
      specs: DataTypes.TEXT,
      availableAt: DataTypes.DATE,
      capacity: DataTypes.INTEGER,
      addbyUser: DataTypes.STRING,
      updatebyUser: DataTypes.STRING,
      deletebyUser: DataTypes.STRING,
      deleteStatus: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "cars",
    }
  );
  return cars;
};
