const carsService = require("../../../services/carsService");
const jwt = require("jsonwebtoken");

module.exports = {
  list(req, res) {
    carsService
      .list()
      .then(({ data, count }) => {
        const filteredData = data.filter((row) => row.deleteStatus === "false");
        count = filteredData.length;
        res.status(200).json({
          status: "OK",
          data: { posts: filteredData },
          meta: { total: count },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  historyCars(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    if (tokenPayload.type == "member") {
      res.status(200).json({
        message: "Tidak diberi akses",
      });
      return;
    }

    carsService
      .list()
      .then(({ data, count }) => {
        data = data.map((row) => {
          return {
            ...row,
            specs: row.specs.split(","),
          };
        });
        res.status(200).json({
          status: "OK",
          data: { posts: data },
          meta: { total: count },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    if (tokenPayload.type == "member") {
      res.status(200).json({
        message: "Tidak diberi akses",
      });
      return;
    }
    const deleteStatus = "false";
    const addbyUser = tokenPayload.username;

    carsService
      .create({ ...req.body, addbyUser, deleteStatus })
      .then((post) => {
        res.status(201).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    if (tokenPayload.type == "member") {
      res.status(200).json({
        message: "Tidak diberi akses",
      });
      return;
    }
    const updatebyUser = tokenPayload.username;

    carsService
      .update(req.params.id, { ...req.body, updatebyUser })
      .then(() => {
        res.status(200).json({
          status: "OK",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    carsService
      .get(req.params.id)
      .then((post) => {
        res.status(200).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  destroy(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    if (tokenPayload.type == "member") {
      res.status(200).json({
        message: "Tidak diberi akses",
      });
      return;
    }

    const deleteStatus = "true";
    const deletebyUser = tokenPayload.username;
    const data = {
      deleteStatus,
      deletebyUser,
    };

    carsService
      .update(req.params.id, data)
      .then(() => {
        res.status(200).json({
          status: "OK",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
};
