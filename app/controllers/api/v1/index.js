const usersController = require("./usersController");
const carsController = require("./carsController");

module.exports = {
  usersController,
  carsController,
};
