const usersService = require("../../../services/usersService");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const SALT = 10;

function encryptPassword(password) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, SALT, (err, encryptedPassword) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(encryptedPassword);
    });
  });
}

function checkPassword(encryptedPassword, password) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(isPasswordCorrect);
    });
  });
}

function createToken(payload) {
  return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia");
}

module.exports = {
  list(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    if (tokenPayload.type == "superadmin") {
      usersService
        .list()
        .then(({ data, count }) => {
          res.status(200).json({
            status: "OK",
            data: {
              posts: data,
            },
            meta: {
              total: count,
            },
          });
        })
        .catch((err) => {
          res.status(400).json({
            status: "FAIL",
            message: err.message,
          });
        });
    } else {
      res.status(200).json({
        message: "Tidak diberi akses",
      });
      return;
    }
  },

  async create(req, res) {
    const username = req.body.username;
    const encryptedPassword = await encryptPassword(req.body.password);
    const type = "member";
    const data = {
      username,
      encryptedPassword,
      type,
    };

    usersService
      .create(data)
      .then((post) => {
        res.status(201).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async update(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    if (tokenPayload.type == "superadmin") {
      const username = req.body.username;
      const encryptedPassword = await encryptPassword(req.body.password);
      const updateby = tokenPayload.username;
      const data = {
        username,
        encryptedPassword,
        updateby,
      };

      usersService
        .update(req.params.id, data)
        .then(() => {
          res.status(200).json({
            status: "OK",
          });
        })
        .catch((err) => {
          res.status(422).json({
            status: "FAIL",
            message: err.message,
          });
        });
    } else {
      res.status(200).json({
        message: "Tidak diberi akses",
      });
      return;
    }
  },

  async approveAdminRole(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    if (tokenPayload.type == "superadmin") {
      const updateby = tokenPayload.username;
      const type = "admin";
      const data = {
        type,
        updateby,
      };

      usersService
        .update(req.params.id, data)
        .then(() => {
          res.status(200).json({
            status: "OK",
          });
        })
        .catch((err) => {
          res.status(422).json({
            status: "FAIL",
            message: err.message,
          });
        });
    } else {
      res.status(200).json({
        message: "Tidak diberi akses",
      });
      return;
    }
  },

  show(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    if (tokenPayload.type == "superadmin") {
      usersService
        .get(req.params.id)
        .then((post) => {
          res.status(200).json({
            status: "OK",
            data: post,
          });
        })
        .catch((err) => {
          res.status(422).json({
            status: "FAIL",
            message: err.message,
          });
        });
    } else {
      res.status(200).json({
        message: "Tidak diberi akses",
      });
      return;
    }
  },

  showCurrent(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    usersService
      .get(tokenPayload.id)
      .then((post) => {
        res.status(200).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  destroy(req, res) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split("Bearer ")[1];
    const tokenPayload = jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia"
    );
    if (tokenPayload.type == "superadmin") {
      usersService
        .delete({
          where: {
            id: req.params.id,
          },
        })
        .then(() => {
          res.status(200).json({
            status: "Success",
            data: post,
          });
        })
        .catch((err) => {
          res.status(422).json({
            status: "FAIL",
            message: err.message,
          });
        });
    } else {
      res.status(200).json({
        message: "Tidak diberi akses",
      });
      return;
    }
  },

  async login(req, res) {
    const username = req.body.username;
    const password = req.body.password;

    const user = await usersService.login(username);

    if (!user) {
      res.status(404).json({
        message: "Username tidak ditemukan",
      });
      return;
    }

    const isPasswordCorrect = await checkPassword(
      user.encryptedPassword,
      password
    );

    if (!isPasswordCorrect) {
      res.status(401).json({
        message: "Password salah!",
      });
      return;
    }

    const token = createToken({
      id: user.id,
      username: user.username,
      type: user.type,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });

    res.status(201).json({
      id: user.id,
      username: user.username,
      type: user.type,
      token,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });
  },

  async authorize(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );

      req.user = await usersService.get(tokenPayload.id);
      next();
    } catch (err) {
      console.error(err);
      res.status(401).json({
        message: "Unauthorized",
      });
    }
  },
};
