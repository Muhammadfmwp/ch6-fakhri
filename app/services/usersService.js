const usersRepository = require("../repositories/usersRepository");

module.exports = {
  create(requestBody) {
    return usersRepository.create(requestBody);
  },

  update(id, requestBody) {
    return usersRepository.update(id, requestBody);
  },

  approveAdminRole(id) {
    return usersRepository.approveAdminRole(id);
  },

  delete(id) {
    return usersRepository.delete(id);
  },

  async list() {
    try {
      const posts = await usersRepository.findAll();
      const postCount = await usersRepository.getTotalusers();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return usersRepository.find(id);
  },

  login(username) {
    return usersRepository.login(username);
  },
};
