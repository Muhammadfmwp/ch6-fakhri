const carsRepository = require("../repositories/carsRepository");

module.exports = {
  create(requestBody) {
    return carsRepository.create(requestBody);
  },

  update(id, requestBody) {
    return carsRepository.update(id, requestBody);
  },

  delete(id) {
    return carsRepository.delete(id);
  },

  async list() {
    try {
      const posts = await carsRepository.findAll();
      const postCount = await carsRepository.getTotalcars();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return carsRepository.find(id);
  },
};
