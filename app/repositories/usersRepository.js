const { users } = require("../models");

module.exports = {
  async create(createArgs) {
    return users.create(createArgs);
  },

  update(id, updateArgs) {
    return users.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  approveAdminRole(id, updateArgs) {
    return users.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return users.destroy(id);
  },

  find(id) {
    return users.findByPk(id);
  },

  findAll() {
    return users.findAll();
  },

  getTotalusers() {
    return users.count();
  },

  login(username) {
    return users.findOne({
      where: { username },
    });
  },
};
