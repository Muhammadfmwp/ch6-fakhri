"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn("users", "updateby", {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn("cars", "addbyUser", {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn("cars", "updatebyUser", {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn("cars", "deletebyUser", {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn("cars", "deleteStatus", {
        type: Sequelize.STRING,
      }),
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  },
};
