"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return Promise.all([
      //   queryInterface.addColumn("cars", "plate", {
      //     type: Sequelize.STRING,
      //   }),
      //   queryInterface.addColumn("cars", "manufacture", {
      //     type: Sequelize.STRING,
      //   }),
      //   queryInterface.addColumn("cars", "capacity", {
      //     type: Sequelize.INTEGER,
      //   }),
      //   queryInterface.addColumn("cars", "description", {
      //     type: Sequelize.TEXT,
      //   }),
      //   queryInterface.addColumn("cars", "transmission", {
      //     type: Sequelize.STRING,
      //   }),
      //   queryInterface.addColumn("cars", "year", {
      //     type: Sequelize.STRING,
      //   }),
      //   queryInterface.addColumn("cars", "options", {
      //     type: Sequelize.STRING,
      //   }),
      //   queryInterface.addColumn("cars", "specs", {
      //     type: Sequelize.TEXT,
      //   }),
      queryInterface.addColumn("users", "updateby", {
        type: Sequelize.STRING,
      }),
    ]);
  },

  async down(queryInterface, Sequelize) {
    /*return queryInterface.removeColumn("Todo", "completed");*/
  },
};
