"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("users", [
      {
        username: "superadmin",
        encryptedPasswword: "superadmin",
        type: "superadmin",
      },
      {
        username: "admin",
        encryptedPasswword: "admin",
        type: "admin",
      },
      {
        username: "member",
        encryptedPasswword: "member",
        type: "member",
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkdelete("users", {}, null);
  },
};
